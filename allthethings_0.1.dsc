Format: 1.0
Source: allthethings
Binary: allthethings
Architecture: amd64
Version: 0.1
Maintainer: badsectorlabs <allthethings@badsectorlabs.com>
Homepage: https://gitlab.com/badsectorlabs/allthethings
Standards-Version: 3.9.5
Vcs-Git: git:///gitlab.com/badsectorlabs/allthethings.git
Build-Depends: debhelper (>= 9)
Package-List:
 allthethings deb utils optional arch=amd64
Checksums-Sha1:
 653239a20caa710dcb8b6673c9f9fe425a3ec264 999530 allthethings_0.1.tar.gz
Checksums-Sha256:
 afb6b4493d1ae82800b3e0fb69cd930577a3f3601c6b89037b86c0877a1813d1 999530 allthethings_0.1.tar.gz
Files:
 5c7da59449063d71c26e774d662dfe77 999530 allthethings_0.1.tar.gz
