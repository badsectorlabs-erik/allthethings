# Build with no signature then sign by hand, because why not?

cd /root/allthethings_dev/allthethings-0.1 
dpkg-buildpackage -us -uc
cd ..
dpkg-sig -g '--no-tty --passphrase-file gpg_pass' --sign builder allthethings_0.1_amd64.deb
cp allthethings_0.1_amd64.deb kali/
dpkg-scanpackages kali /dev/null | gzip -9c > Packages.gz
rm Packages
gunzip -k Packages.gz 
./make_Release.sh
rm -f Release.gpg
gpg --no-tty --armor --detach-sign --passphrase-file gpg_pass --output Release.gpg --digest-algo SHA512 Release
