# What are the things?

* ## Custom CTF Wallpaper (https://gitlab.com/badsectorlabs/allthethings/raw/master/allthethings-0.1/wallpaper.jpg)
  * Ascii table, linux memory layout, register map, and more!

* ## Firefox start page (https://gitlab.com/badsectorlabs/allthethings/blob/master/allthethings-0.1/startpage/index.html)
  * Links to all the things!

* ## pwndbg (https://github.com/pwndbg/pwndbg)
  * Make GDB suck less
  * Includes Unicorn (https://github.com/unicorn-engine/unicorn) and Capstone (http://www.capstone-engine.org/)

* ## bpython (http://www.bpython-interpreter.org/)
  * python + curses = winning

* ## Crypto Tools
  * rsatool (https://github.com/ius/rsatool)
  * xortool (https://github.com/hellman/xortool)

* ## binjitsu (https://github.com/binjitsu/binjitsu/)
  * Python CTF/exploit development toolkit

* ## Bash Alises
  * ll: ls -lart
  * rshell: Print the commands for a reverse shell, specificy a language, and 
optionally an IP and port to call back to, or call with just a language for 
your IP and a random port


# Installation

```
echo "deb http://apt.badsectorlabs.com kali/" >> /etc/apt/sources.list
wget -O - https://apt.badsectorlabs.com/apt.badsectorlabs.gpg.pub | apt-key add -
apt-get update
apt-get install allthethings
````
