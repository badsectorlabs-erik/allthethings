#!/bin/bash
cat > Release << EOF
Archive: Bad Sector Labs
Origin: Kali
Label: Bad Sector Labs apt Repository for Kali Linux
Date: $(date -R -u)
Architecture: amd64
SHA256:
EOF
printf ' '$(sha256sum Packages.gz | cut --delimiter=' ' --fields=1)' %16d Packages.gz\n' $(wc --bytes Packages.gz | cut --delimiter=' ' --fields=1) >> Release
printf ' '$(sha256sum Packages | cut --delimiter=' ' --fields=1)' %16d Packages\n' $(wc --bytes Packages | cut --delimiter=' ' --fields=1) >> Release
